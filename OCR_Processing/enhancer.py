import cv2

def enhancer(img):
    ret, th = cv2.threshold(img,
                            0,
                            255,
                            cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    nimage = cv2.bitwise_not(th)

    return  nimage