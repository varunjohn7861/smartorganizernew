import cv2
import numpy as np
from OCR_Processing.PreProcessing import pre_processing
import glob
import json
import os
from OCR_Processing.Check import *
import io
from OCR_Processing.database import insert_records
from OCR_Processing.text_recognition import detect_text
from OCR_Processing.template_matcher import template_macther
from OCR_Processing.feature_generator import feature_generator
from OCR_Processing.feature_matcher import feature_matcher
from OCR_Processing.perspective_transformer import perspective_transformer

def match(imgs_match, path):
    data = {}
    pages_data = {}
    value_changed = []
    for index,img_match in enumerate(imgs_match):
        page_data = {}
        templates = glob.glob('./template_images/*')

        template_match = template_macther(templates,img_match)

        template_match_sort = sorted(template_match, key=lambda k: k['match'], reverse=True)
        if glob.glob('./templates/'+"_".join(template_match_sort[0]['name'].split("_")[:-1])+'*'):
            img2 = cv2.imread(glob.glob('./templates_images/'+template_match_sort[0]['name']+'*')[0])
            height, width, _ = img2.shape
            kp2, des2 = feature_generator(img_match, 2000)
            kp3, des3 = feature_generator(img2, 2000)
            match_new = feature_matcher(des3, des2)
            match_new.sort(key=lambda x: x.distance)
            good = match_new[:int(len(match_new) * (90 / 100))]

            srcPoints = np.float32([kp2[m.queryIdx].pt for m in good]).reshape(-1, 1, 2)
            dstPoints = np.float32([kp3[m.trainIdx].pt for m in good]).reshape(-1, 1, 2)

            imgscan = perspective_transformer(srcPoints, dstPoints, width, height, img_match)

            with open(glob.glob('./templates/'+"_".join(template_match_sort[0]['name'].split("_")[:-1])+'*')[0]) as temp_json:
                roi = json.load(temp_json)
            for x in roi['page_'+template_match_sort[0]['name'].split("_")[-1].split(".")[0]]:
                img_crop = imgscan[x['point_1'][1]:x['point_2'][1], x['point_1'][0]:x['point_2'][0]]
                cv2.imwrite("temp.jpg",img_crop)
                if x['type'] == 'check_box':
                    img_cb = cv2.imread("temp.jpg",cv2.IMREAD_GRAYSCALE)
                    imgThresh = cv2.threshold(img_cb,170,255, cv2.THRESH_BINARY_INV)[1]
                    total_pixels = cv2.countNonZero(imgThresh)
                    if total_pixels>1000:
                        data[x['name']] = 'True'
                        page_data[x['name']] = 'True'
                    else:
                        data[x['name']] = 'False'
                        page_data[x['name']] = 'False'

                else:
                    with io.open("temp.jpg", 'rb') as image_file:
                        content = image_file.read()
                    response = detect_text(content)
                    texts = response.text_annotations
                    if "completeness check" in x["checks"]:
                        try:
                            texts[0].description.strip()
                        except:
                            if "required field" in x["checks"]:
                                data[x['name']] = "Fail - completeness check"
                                page_data[x['name']] = "Fail - completeness check"

                    if not x['name'] in page_data.keys() and "validity check" in x["checks"]:
                        try:
                            text_data = texts[0].description.strip()
                            valid_result = validity_check(x['type'], text_data)
                            if isinstance(valid_result, str):
                                if 'Exception - 'in valid_result:
                                    data[x['name']] = "Fail - validity check: " + valid_result.replace("Exception - ","")
                                    page_data[x['name']] = "Fail - validity check: " + valid_result.replace("Exception - ","")
                        except:
                            data[x['name']] = "Fail - validity check"
                            page_data[x['name']] = "Fail - validity check"

                    if not x['name'] in page_data.keys() and "consistency check" in x["checks"]:
                        try:
                            text_data = texts[0].description.strip()
                            data[x['name']] = consistency_check(x['type'], text_data)
                            page_data[x['name']] = consistency_check(x['type'], text_data)
                        except:
                            data[x['name']] = "Fail - consistency check"
                            page_data[x['name']] = "Fail - consistency check"

                    if not x['name'] in page_data.keys():
                        try:
                            data[x['name']] = texts[0].description.strip()
                            page_data[x['name']] = texts[0].description.strip()
                        except:
                            data[x['name']] = ""
                            page_data[x['name']] = ""

                    if x['name'] == "Manufacturer_name":
                        if "confirmity check" in x["checks"] and isinstance(page_data[x['name']],str):
                            if "Fail -" not in page_data[x['name']]:
                                check_result = conformity_check(page_data[x['name']])
                                if check_result == "Fail":
                                    if "accuracy check" in x["checks"]:
                                        check_result_acc, status = accuracy_check(page_data[x['name']])
                                        if check_result_acc == "Fail":
                                            page_data[x['name']] = "Fail - confirmity check"
                                            data[x['name']] = "Fail - confirmity check"
                                        else:
                                            if status == "change":
                                                value_changed.append(x['name'])
                                            page_data[x['name']] = check_result_acc
                                            data[x['name']] = check_result_acc
                                    else:
                                        page_data[x['name']] = "Fail - confirmity check"
                                        data[x['name']] = "Fail - confirmity check"

                        elif "accuracy check" in x["checks"] and isinstance(page_data[x['name']],str):
                            if "Fail -" not in page_data[x['name']]:
                                check_result, status = accuracy_check(page_data[x['name']])
                                print(check_result)
                                if check_result == "Fail":
                                    page_data[x['name']] = "Fail - confirmity check"
                                    data[x['name']] = "Fail - confirmity check"
                                else:
                                    if status == "change":
                                        value_changed.append(x['name'])
                                    page_data[x['name']] = check_result
                                    data[x['name']] = check_result


                os.remove("temp.jpg")
        page_data['values_changed'] = value_changed
        pages_data["page_"+str(index+1)] = {"template": template_match_sort[0]['name']+".json", "page_data": page_data}


    temp_dict = {"file_name":path.split("/")[-1], "data": pages_data}
    insert_records('smartorganizer_page_data', "_".join(template_match_sort[0]['name'].split("_")[:-1]), temp_dict)

    if len([y for y in [x for x in data.values() if isinstance(x,str)] if 'Fail' in y]) != 0:
        return "Incorrect File", "_".join(template_match_sort[0]['name'].split("_")[:-1])

    else:
        insert_records('smartorganizer',"_".join(template_match_sort[0]['name'].split("_")[:-1]),data)
        return "Correct File", "_".join(template_match_sort[0]['name'].split("_")[:-1])

def get_files(path):
        img = pre_processing(path)
        file_result = match(img, path)
        return file_result