from dateutil.parser import parse
import re
import locale
from pymongo import MongoClient
from Levenshtein import distance

locale.setlocale(locale.LC_NUMERIC,'de_DE.utf8')

def validity_check(type , data):
    if type.lower() == 'date':
        try:
            return parse(data.replace(" ",""))
        except Exception as e:
            return "Exception - " + str(e)
    if type.lower() == 'number':
        try:
            return float(locale.atof(data))
        except ValueError:
            return "Exception - " + "data contains string in number type"

    if type.lower() == 'string':
        if not data.isdecimal():
            return data
        else:
            return "Exception - " + "data contains all numbers"
    if type.lower() == 'currency':
        try:
            data = str(locale.atof(re.sub("[^0-9.,]","",data)))
            if '.' in data:
                if len(data.rsplit(".")[-1])in [1,2]:
                    return data
                else:
                    return "Exception - " + "invalid number after decimal place for currency"
            else:
                return True
        except:
            return "Exception - " + "invalid currency value"


def consistency_check(type,data):
    if type.lower() == 'date':
        return str(parse(data.replace(" ","")))

    if type.lower() == 'number':
        return float(locale.atof(data))

    if type.lower() == 'string':
        return re.sub("[^A-Za-z0-9 \-\n]","",data).lower()

    if type.lower() == 'currency':
        return float(locale.atof(re.sub("[^0-9.,]","",data)))

def conformity_check(data):
    m_client = MongoClient()
    result = m_client.Conformity.ADAC.find({}, {'manufacturer': 1, '_id': 0})
    result = [x['manufacturer'].lower() for x in result]
    if data.lower() not in result:
        return "Fail"

def accuracy_check(data):
    m_client = MongoClient()
    result = m_client.Conformity.ADAC.find({}, {'manufacturer': 1, '_id': 0})
    result = [x['manufacturer'].lower() for x in result]

    ld = [{'manufacturer': x, 'distance':distance(data.lower(),x)} for x in result]
    ld_sorted = sorted(ld, key=lambda k: k['distance'])
    if ld_sorted[0]['distance'] > 1:
        return 'Fail', ""
    elif ld_sorted[0]['distance'] == 1:
        return ld_sorted[0]['manufacturer'], "change"
    else:
        return data, "nochange"