from pymongo import MongoClient

def insert_records(database, collection, data):
    mon_client = MongoClient()
    db = mon_client[database]
    collection = db[collection]
    collection.insert(data)
    mon_client.close()
