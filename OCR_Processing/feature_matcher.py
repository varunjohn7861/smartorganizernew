import cv2

def feature_matcher(des1, des2, crossCheck=False):
    bf_match = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=crossCheck)
    match = bf_match.match(des2, des1)
    return match