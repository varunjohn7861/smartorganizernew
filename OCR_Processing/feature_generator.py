import cv2

def feature_generator(img, n_features = 5000):
    orb = cv2.ORB_create(n_features)
    kp, des = orb.detectAndCompute(img, None)
    return kp, des