import glob
import cv2
import json
import os
from win32api import GetSystemMetrics
from OCR_Processing.PreProcessing import *

circles = []
counter = 0
counter2 = 0
point1 = []
point2 = []
mypoints = []


def mousePoints(event,x,y,flags,params):
    global counter, point1, point2, counter2, circles, mycolor
    if event == cv2.EVENT_LBUTTONDOWN:
        if counter ==0:
            point1 = int(x//scale), int(y//scale)
            counter +=1
            mycolor = (0,0,255)
        elif counter==1:
            point2 = int(x // scale), int(y // scale)
            type = input('Enter Type')
            name = input('Enter Name')
            mypoints.append([point1,point2,name, type])
            counter=0
        circles.append([x,y,mycolor])
        counter2 += 1



def getroi(img):
    global scale
    scale = GetSystemMetrics(1)/img.shape[1]
    img = cv2.resize(img, (0, 0), None, scale, scale, interpolation=cv2.INTER_AREA)

    while True:
        for x,y,color in circles:
            cv2.circle(img, (x,y),3,color, cv2.FILLED)
        for x in mypoints:
            point_1 = tuple(int(x * scale) for x in x[0])
            point_2 = tuple(int(x * scale) for x in x[1])
            # point_1 = tuple(int(x) for x in x[0])
            # point_2 = tuple(int(x) for x in x[1])
            cv2.rectangle(img, point_1, point_2, (0, 0, 255), 2)

        # cv2.namedWindow('original', cv2.WINDOW_NORMAL)
        cv2.imshow("original", img)
        cv2.setMouseCallback("original", mousePoints)
        if cv2.waitKey(1) == ord('s'):
            break
    cv2.destroyAllWindows()

    return [{'name':x[2] ,'type':x[3], 'point_1': x[0], 'point_2': x[1]} for x in mypoints]


templates = glob.glob('./template_images/*')
al_roi = os.listdir('./template/')

templates = [x for x in templates if x.split('\\')[-1].split('.')[0] + '.json' not in al_roi]

for x in templates:
    page_l = {}
    imgs = pre_processing(x)
    for index,img in enumerate(imgs):
        roi = getroi(cv2.imread(x))
        page_l['page_'+str(index+1)] = roi
    with open('./template/'+ x.split('\\')[-1].split('.')[0]+'.json', 'a+') as file:
        json.dump(page_l, file)
    circles = []
    counter = 0
    counter2 = 0
    point1 = []
    point2 = []
    mypoints = []
