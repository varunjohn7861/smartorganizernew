# from bs4 import BeautifulSoup as bs
# import requests
# import json
#
# url = "https://www.adac.de/rund-ums-fahrzeug/autokatalog/marken-modelle/?filter=ONLY_RECENT&sort=SORTING_DESC"
# req = requests.get(url)
# soup = bs(req.content, 'html.parser')
#
# manufacturer_list = []
# result1 = soup.find('div',{'class':'sc-hFFCpj sc-cnysau sc-bruRAN COOlq eEUTDy dDHdgU'})
# # print(result1)
# result2 = result1.find_all('a', {'class':'sc-TtZnY dngUwA sc-jHNicF sc-jldrjA bMmwdY cHedBP'})
# # print(result2)
# for x in result2:
#     manufacturer_list.append({'manufacturer':json.loads(x.get('data-tracking'))['click']['label'], 'href' : x.get('href')})
# all_list = []
# for x in manufacturer_list:
#     series_list = []
#     req = requests.get('https://www.adac.de'+ x['href'])
#     soup = bs(req.content, 'html.parser')
#     result1 = soup.find('div', {'class': 'sc-hFFCpj sc-cnysau sc-hQbnUn COOlq eEUTDy hwyIfC'})
#     result2 = result1.find_all('a', {'class': 'sc-TtZnY dngUwA sc-jHNicF sc-hiMZNx bMmwdY iSutCo'})
#
#     for y in result2:
#         # print(y)
#         series_list.append(json.loads(y.get('data-tracking'))['click']['label'])
#
#     all_list.append({'manufacturer': x['manufacturer'], 'series_list': series_list})
#
# with open('C:/Users/varun/Downloads/ADAC/confirmity.json','w+') as file:
#     json.dump(all_list,file)
#
# #     print(x.find('a', {'class':'sc-TtZnY dngUwA sc-jHNicF sc-jldrjA bMmwdY cHedBP'}).get('label'))
import json

from pymongo import MongoClient

client = MongoClient()
db = client['Conformity']
collection = db['ADAC']

with open('C:/Users/varun/Downloads/ADAC/confirmity.json','r+') as file:
    data = json.load(file)

collection.insert_many(data)