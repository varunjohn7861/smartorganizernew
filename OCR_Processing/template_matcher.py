import cv2
from OCR_Processing.feature_generator import feature_generator
from OCR_Processing.feature_matcher import feature_matcher

def template_macther(templates,img_match):
    template_match = []
    for temp_img in templates:
        imgt = cv2.imread(temp_img)
        kp1, des1 = feature_generator(imgt)
        kp2, des2 = feature_generator(img_match)
        match = feature_matcher(des1, des2, crossCheck=True)
        template_match.append({'name': temp_img.split('/')[-1].split('.')[0], 'match': len(match)})

    return template_match