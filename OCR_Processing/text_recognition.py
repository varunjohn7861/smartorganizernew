from google.cloud import vision
import os

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = "./varuns-project-309117-fcca503767c3.json"
client = vision.ImageAnnotatorClient()

def detect_text(content):
    image = vision.Image(content=content)
    response = client.text_detection(image=image, image_context={"language_hints": ["de"]})
    return response