from pdf2image import convert_from_path
from docx2pdf import convert
import cv2
import numpy as np

def pre_processing(file_path):
    images = checkfiletype(file_path)
    return images

def checkfiletype(input):
    if input.endswith('.pdf'):
        images = pdftoimg(input)
        images = [np.asarray(x) for x in images]
        images = [x[:, :, ::-1].copy() for x in images]
        return images
    elif input.endswith('.docx'):
        images = doctoimg(input)
        return images
    else:
        return cv2.imread(input)

def pre_processing_pil(file_path):
    images = checkfiletype_pil(file_path)
    return images

def checkfiletype_pil(input):
    if input.endswith('.pdf'):
        images = pdftoimg(input)
        return images
    elif input.endswith('.docx'):
        images = doctoimg(input)
        return images
    else:
        return cv2.imread(input)

def pdftoimg(input):
    pages = convert_from_path(input, 500)
    return pages

def doctoimg(input):
    convert(input, "C:/SRH/Thesis/test_img/" + input.split("/")[-1].split(".")[0] + ".pdf")
    pages = convert_from_path("C:/SRH/Thesis/test_img/" + input.split("/")[-1].split(".")[0] + ".pdf", 500)
    i = 0
    for page in pages:
        i += 1
        page.save("C:/SRH/Thesis/test_img/"+ input.split("/")[-1].split(".")[0] + "_" + str(i) + ".jpg", 'JPEG')
