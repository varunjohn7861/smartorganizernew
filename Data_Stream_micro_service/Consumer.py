from kafka import KafkaConsumer




KafkaConsumer(consumer_timeout_ms=1000)
consumer_1 = KafkaConsumer("SmartOrganizerFileName", bootstrap_servers=['localhost:9092'], group_id=None
                         ,
                         auto_offset_reset='earliest')

consumer_2 = KafkaConsumer("SmartOrganizerData", bootstrap_servers=['localhost:9092'], group_id=None
                         ,
                         auto_offset_reset='earliest')


for (msg_1,msg_2) in zip(consumer_1, consumer_2):
    file = open("C:/SRH/Thesis/test/" + msg_1.value.decode(), "wb")
    file.write(msg_2.value)
    file.close()
consumer_1.close()
consumer_2.close()
