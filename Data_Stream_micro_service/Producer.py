from kafka import KafkaProducer
import glob

producer = KafkaProducer(bootstrap_servers='localhost:9092')


for files in glob.iglob("C:/SRH/Thesis/rvl-cdip/images" + '/**/*.tif',recursive=True):

    with open(files.replace("\\","/"),"rb") as f:
        value = f.read()
        print(value)
        producer.send('SmartOrganizerFileName', files.replace("\\","/").split("/")[-1].encode())
        producer.send('SmartOrganizerData', value)

producer.flush()

