from subprocess import PIPE,Popen
import glob

for file in glob.iglob("/mnt/c/SRH/Thesis/rvl-cdip/images" + '/**/*.tif',recursive=True):
    print(file)
    put = Popen(["hadoop", "fs", "-put", file, "hdfs://localhost:9000/user/hdoop/test_batch/"], stdin=PIPE, bufsize=-1)
    put.communicate()