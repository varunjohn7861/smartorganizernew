# SmartOrganizer
### Document Digitization Framework for automatic cleaning and feedback generation

SmartOrganizer is an AI-enabled, cloud deployment ready document digitization framework which can clean uploaded documents and provide feedback on the document.

- Has interactive user interface
- Multi page functionality
- Cloud deployment ready

## Features

- Can convert images, pdfs, word documents into digital documents and store in database
- Can extarct text and give feedback on the text based on data quality checks
- Provides visulization of KPIs which are created based on the extarcted data
- Provides NLP solutions like summarization and question answer generation on extarcted data from document of any size.
- Can automatically clean data based on a knowledge base.

Document digitization is key to transformation of paper based industries to a more sustainable, manageable and cost effective solution. 

## Tech Stack

SmartOrganizer uses a number of tools for the functioning of its framework:

- [Python 3.7](https://www.python.org/downloads/) - for the scripts of the framework
- [Google Cloud Vision](https://cloud.google.com/vision) - as the OCR engine for text recognition
- [Streamlit](https://streamlit.io/) - Markdown parser done right. Fast and easy to extend.

## Architecture
![Architecture of SmartOrganizer](./Images/so_arch.JPG "Architecture of SmartOrganizer")

## Installation

Dillinger requires a linux based cloud machine to run the framework.

Install the dependencies of the framework in a cloud machine and run the framework using following command:

```sh
sudo apt install git -y; git clone https://gitlab.com/varunjohn7861/smartorganizernew.git; cd smartorganizernew; chmod +x setup.sh; ./setup.sh

streamlit run app.py
```

Open the framework usign a webrower using the external IP of the VM and port ```:8501```.

## Author

Varun John