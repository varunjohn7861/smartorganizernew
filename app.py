import glob
import streamlit as st
from OCR_Processing.opencv_forms import get_files
from PIL import ImageDraw
import pandas as pd
import json
from OCR_Processing.PreProcessing import *
from pymongo import MongoClient
import plotly.express as px
from Text_to_Text_Analysis.summarization import summarize
from Text_to_Text_Analysis.question_answer import generate_qaa
import io
from OCR_Processing.text_recognition import detect_text
import re


# @st.cache(show_spinner=False, suppress_st_warning=True)
def execute_create_template(file_json):

    page_l = {}
    for key, value in file_json.items():
        t_list = []
        resp_ms = {}
        if value:
            st.write("Please enter details for "+ key)
            for x in value:

                if x['type'] == 'currency':
                    resp_ms[x['name']] = st.multiselect("Check options for field " + x['name'],
                                                        ('completeness check', 'consistency check', 'validity check','required field'))

                elif x['type'] == 'date':
                    resp_ms[x['name']] = st.multiselect("Check options for field " + x['name'],
                                                        ('completeness check', 'consistency check', 'validity check','required field'))

                elif x['type'] == 'string':
                    resp_ms[x['name']] = st.multiselect("Check options for field " + x['name'],
                                                        ('completeness check', 'consistency check', 'validity check',
                                                         'confirmity check', 'accuracy check','required field'))

                elif x['type'] == 'number':
                    resp_ms[x['name']] = st.multiselect("Check options for field " + x['name'],
                                                        ('completeness check', 'consistency check', 'validity check',
                                                         'confirmity check','required field'))
                elif x['type'] == 'check_box':
                    resp_ms[x['name']] = []
                x['checks'] = resp_ms[x['name']]

                t_list.append(x)
        page_l[key] = t_list

    resp = st.button("Submit")
    if resp:
        st.session_state.ct_resp = False
        st.session_state.ct_success = True
        return page_l

# @st.cache(show_spinner=False, suppress_st_warning=True)
def execute_edit_template(file_json, temp_name):

    page_l = {}
    for key, value in file_json.items():
        t_list = []
        resp_ms_1 = {}
        resp_but = {}
        if value:
            st.write("Please enter details for "+ key)
            for x in value:
                if x['type'] == 'currency':
                    resp_ms_1[key+x['name']] = st.multiselect("Check options for field " + x['name'],
                                                        ('completeness check', 'consistency check', 'validity check','required field'), default=x['checks'], key=temp_name+key+x['name'])

                elif x['type'] == 'date':
                    resp_ms_1[key+x['name']] = st.multiselect("Check options for field " + x['name'],
                                                        ('completeness check', 'consistency check', 'validity check','required field'), default=x['checks'], key=temp_name+key+x['name'])

                elif x['type'] == 'string':
                    resp_ms_1[key+x['name']] = st.multiselect("Check options for field " + x['name'],
                                                        ('completeness check', 'consistency check', 'validity check', 'confirmity check','required field', 'accuracy check'), default=x['checks'], key=temp_name+key+x['name'])

                elif x['type'] == 'number':
                    resp_ms_1[key+x['name']] = st.multiselect("Check options for field " + x['name'],
                                                        ('completeness check', 'consistency check', 'validity check', 'confirmity check','required field'), default=x['checks'], key=temp_name+key+x['name'])
                elif x['type'] == 'check_box':
                    resp_ms_1[key+x['name']] = []

                x['checks'] = resp_ms_1[key+x['name']]

                t_list.append(x)
        page_l[key] = t_list

    resp_but[temp_name] = st.button("Submit", key=temp_name)
    if resp_but[temp_name]:
        st.session_state.edit_template_file[temp_name] = True
        return page_l

# @st.cache(show_spinner=False, suppress_st_warning=True)
def execute(files):
    progress = 0
    file_df = pd.DataFrame(columns=['file_name', 'file_status', 'file_link', 'template'])
    for file in files:
        with st.spinner("processing "+ file.name):
            result, template = get_files("./uploaded_files/"+ file.name)
        progress += int(100/len(files))
        my_bar.progress(progress)
        file_df = file_df.append({'file_name':file.name, 'file_status':result, 'file_link': "./uploaded_files/"+ file.name, 'template': template}, ignore_index=True)

    my_bar.progress(100)
    st.success("Done")

    expander = {}
    if not file_df.empty:
        df_correct = file_df[file_df['file_status']=='Correct File']
        df_incorrect = file_df[file_df['file_status']=='Incorrect File']

        if not df_correct.empty:
            st.markdown("<h3 style='text-align: center; color: green;'>Clean Files: "+ str(df_correct.shape[0]) +"</h3>", unsafe_allow_html=True)
            for index, row in df_correct.iterrows():
                m_client = MongoClient()
                db = m_client["smartorganizer_page_data"]
                collection = db[row['template']]
                expander[row['file_name']] = st.expander(label=row['file_name'])
                with expander[row['file_name']]:

                    # with open('./templates/'+row['template']) as roi_json_file:
                    #     roi_json = json.load(roi_json_file)
                    temp_json_doc = collection.find({"file_name": row['file_name']})[0]['data']
                    img_corr = pre_processing_pil("./uploaded_files/"+row['file_name'])
                    for index,img in enumerate(img_corr):
                        draw = ImageDraw.Draw(img)
                        template_json = temp_json_doc['page_'+str(index+1)]['page_data']
                        if template_json:
                            with open("./templates/" + temp_json_doc['page_'+str(index+1)]['template'].split("_")[0]+".json") as roi_json_file:
                                roi_json = json.load(roi_json_file)['page_'+temp_json_doc['page_'+str(index+1)]['template'].split("_")[-1].split(".")[0]]
                            for x in roi_json:
                                if isinstance(template_json[x['name']],str):
                                    if "Fail - " in template_json[x['name']]:
                                        draw.rectangle((x['point_1'][0],x['point_1'][1], x['point_2'][0],x['point_2'][1]), outline="red", width=10)
                                        # font = ImageFont.truetype("sans-serif.ttf", 16)
                                        # draw.text((x['point_2'][0],x['point_1'][1]-25),template_json[x['name']],fill="red")
                                    else:
                                        if x['name'] in template_json['values_changed']:
                                            draw.rectangle(
                                                (x['point_1'][0], x['point_1'][1], x['point_2'][0], x['point_2'][1]),
                                                outline="orange", width=10)
                                        else:
                                            draw.rectangle((x['point_1'][0], x['point_1'][1], x['point_2'][0], x['point_2'][1]),
                                                      outline=(0,100,0), width=10)
                                else:
                                    if x['name'] in template_json['values_changed']:
                                        draw.rectangle(
                                            (x['point_1'][0], x['point_1'][1], x['point_2'][0], x['point_2'][1]),
                                            outline="orange", width=10)
                                    else:
                                        draw.rectangle((x['point_1'][0], x['point_1'][1], x['point_2'][0], x['point_2'][1]),
                                                   outline=(0,100,0), width=10)

                        st.image(img)
                    # st.write(row['file_name'])
                m_client.close()

        if not df_incorrect.empty:
            st.markdown("<h3 style='text-align: center; color: red;'>Unclean Files: "+ str(df_incorrect.shape[0]) +"</h3>", unsafe_allow_html=True)
            for index, row in df_incorrect.iterrows():
                fail_reasons = []
                m_client = MongoClient()
                db = m_client["smartorganizer_page_data"]
                collection = db[row['template']]
                expander[row['file_name']] = st.expander(label=row['file_name'])
                with expander[row['file_name']]:

                    temp_json_doc = collection.find({"file_name": row['file_name']})[0]['data']
                    # with open('./templates/' + row['template']) as roi_json_file:
                    #     roi_json = json.load(roi_json_file)
                    img_corr = pre_processing_pil("./uploaded_files/" + row['file_name'])
                    for index, img in enumerate(img_corr):
                        draw = ImageDraw.Draw(img)
                        template_json = temp_json_doc['page_' + str(index + 1)]['page_data']
                        fail_reasons = []
                        if template_json:
                            with open("./templates/" + temp_json_doc['page_' + str(index + 1)]['template'].split("_")[
                                0] + ".json") as roi_json_file:
                                roi_json = json.load(roi_json_file)['page_' + temp_json_doc['page_' + str(index + 1)][
                                    'template'].split("_")[-1].split(".")[0]]
                            for x in roi_json:
                                if isinstance(template_json[x['name']], str):
                                    if "Fail - " in template_json[x['name']]:
                                        fail_reasons.append(template_json[x['name']])
                                        draw.rectangle((x['point_1'][0], x['point_1'][1], x['point_2'][0], x['point_2'][1]),
                                                       outline="red", width=10)
                                        # font = ImageFont.truetype("FreeMono.ttf", 16)
                                        # draw.text((x['point_2'][0], x['point_1'][1] - 25), template_json[x['name']], fill="red", font=font)
                                    else:
                                        if x['name'] in template_json['values_changed']:
                                            draw.rectangle(
                                                (x['point_1'][0], x['point_1'][1], x['point_2'][0], x['point_2'][1]),
                                                outline="orange", width=10)
                                        else:
                                            draw.rectangle((x['point_1'][0], x['point_1'][1], x['point_2'][0], x['point_2'][1]),
                                                       outline=(0, 100, 0), width=10)
                                else:
                                    if x['name'] in template_json['values_changed']:
                                        draw.rectangle(
                                            (x['point_1'][0], x['point_1'][1], x['point_2'][0], x['point_2'][1]),
                                            outline="orange", width=10)
                                    else:
                                        draw.rectangle((x['point_1'][0], x['point_1'][1], x['point_2'][0], x['point_2'][1]),
                                                   outline=(0, 100, 0), width=10)
                        st.markdown("<h4 style='text-align: center; color: red;'>" + ", ".join(fail_reasons)
                                    + "</h4>", unsafe_allow_html=True)
                        st.image(img)
                m_client.close()



        p_chart_labels = ['Correct Files', 'Incorrect Files']
        p_chartsizes = [df_correct.shape[0],df_incorrect.shape[0]]
        df_new = pd.DataFrame(list(zip(p_chart_labels,p_chartsizes)),columns=['status','count'])
        fig = px.pie(df_new,values='count',names='status', color_discrete_sequence=['red','green'],
                     labels={'status':'','count':'No. of files'},hole=0.5)
        fig.update_layout(title_text='Percentage of correct and incorrect files', title_x=0.5)
        fig.update_traces(pull=[0.1,0],marker=dict(line=dict(color='#000000', width=3)))
        st.plotly_chart(fig, use_container_width=True)

        bar_chart_values = {}
        m_client = MongoClient()
        db = m_client["smartorganizer_page_data"]
        collection = db['ADAC']
        result = collection.find({}, {"data": 1, "_id": 0})

        for x in result:
            for key_r in x['data'].keys():
                for key, value in x['data'][key_r]['page_data'].items():
                    if isinstance(value, str):
                        if "Fail -" in value:
                            if key in bar_chart_values.keys():
                                bar_chart_values[key] += 1
                            else:
                                bar_chart_values[key] = 1

        df = pd.DataFrame(columns=['field_name', 'count'])

        for key, value in bar_chart_values.items():
            df = df.append({'field_name': key, 'count': value}, ignore_index=True)

        fig = px.bar(df, x='field_name', y='count',color='count',text='count')
        fig.update_layout(title_text='Fields with most incorrect values', title_x=0.5)
        st.plotly_chart(fig, use_container_width=True)


my_page = st.sidebar.radio('Application Navigation', ['Create template','View Templates','Extract Data','Text Analysis'])

if my_page == 'Create template':
    st.title(""" Smart Organizer""")
    template_file = st.file_uploader("upload files for template creation", type=['.json'])

    if 'template_file' not in st.session_state:
        st.session_state.template_file = False

    if 'ct_success' not in st.session_state:
        st.session_state.ct_success = False


    if template_file or st.session_state.template_file:
        st.session_state.template_file = True
        with open("./templates/" + template_file.name, "wb") as f:
            f.write(template_file.getbuffer())

        st.write("### File uploaded")

        ct_resp = st.button('Create Template')

        if 'ct_resp' not in st.session_state:
            st.session_state.ct_resp = False

        if ct_resp or st.session_state.ct_resp:
            st.session_state.ct_resp = True
            with open("./templates/" + template_file.name, "r") as f:
                file_json = json.load(f)
            t_list = execute_create_template(file_json)

            with open("./templates/" + template_file.name, "r+") as f:
                json.dump(t_list,f)

        if st.session_state.ct_success:
            st.success("Template Created")
            st.session_state.template_file = False

elif my_page == 'View Templates':
    st.title(""" Smart Organizer""")
    st.write("### Following are the templates which exist in the system currently.")
    templates_l = glob.glob("./templates/*")
    if 'edit_template_file' not in st.session_state:
        st.session_state.edit_template_file = {}

    expander = {}
    for file in templates_l:
        expander[file.split("\\")[-1].split(".")[0]] = st.expander(label=file.split("\\")[-1].split(".")[0])
        with expander[file.split("\\")[-1].split(".")[0]]:
            img_l = pre_processing_pil(glob.glob("./uploaded_templates/" + file.split("\\")[-1].split(".")[0] + "*")[0])
            with open(glob.glob("./templates/" + file.split("\\")[-1].split(".")[0] + "*")[0]) as roi_json_file:
                temp_json = json.load(roi_json_file)
            for index,img in enumerate(img_l):
                temp_json_page = temp_json['page_'+str(index+1)]
                draw = ImageDraw.Draw(img)
                for x in temp_json_page:
                    draw.rectangle((x['point_1'][0], x['point_1'][1], x['point_2'][0], x['point_2'][1]),
                               outline="green", width=10)
                st.image(img)
            return_json = execute_edit_template(temp_json, file.split("\\")[-1].split(".")[0])

            if st.session_state.edit_template_file:
                with open(glob.glob("./templates/" + file.split("\\")[-1].split(".")[0] + "*")[0],"w") as roi_json_file:
                    json.dump(return_json,roi_json_file)
                st.session_state.edit_template_file = {}

elif my_page == 'Text Analysis':
    st.title(""" Smart Organizer""")
    st.write("### Please enter text for further processing.")

    text_file = st.file_uploader("upload files:", type=['.pdf','.jpg','.doc'])

    if text_file:
        with open("./uploaded_files/" + text_file.name, "wb") as f:
            f.write(text_file.getbuffer())
        st.write("### File uploaded")
        resp_text_func = st.button("Process File")

        if resp_text_func:
            full_qa = []
            with st.spinner("Generating Summary"):
                st.header("Summary:")
                imgs = pre_processing_pil("./uploaded_files/"+text_file.name)
                expander_tf = {}
                full_content = ""
                for index,img in enumerate(imgs):
                    img.save("temp.jpg", "JPEG")
                    with io.open("temp.jpg", 'rb') as image_file:
                        content = image_file.read()
                    response = detect_text(content)
                    texts = response.text_annotations
                    page_summary = summarize(texts[0].description.strip())
                    # if full_content == "":
                    #     full_content = texts[0].description.strip()
                    # else:
                    #     full_content = full_content + ". " + texts[0].description.strip()
                    expander_tf['Page_' + str(index+1)] = st.expander(label='Page_' + str(index+1))
                    with expander_tf['Page_' + str(index+1)]:
                        st.write(page_summary)
                    page_summary = re.sub('[^A-Za-z0-9 .]+', ' ', page_summary)
                    page_summary = re.sub('\.\.+', ' ', page_summary)
                    page_summary = re.sub('  +', ' ', page_summary)
                    qaa = generate_qaa(page_summary)
                    full_qa += qaa



            with st.spinner("Generating FAQs"):

                st.header("FAQs:")
                qa_expander = {}
                for index, qa_dict in enumerate(full_qa):
                    answer = qa_dict['answer']
                    question = qa_dict['question']

                    qa_expander['question_'+ str(index+1)] = st.expander(label=question)
                    with qa_expander['question_'+ str(index+1)]:
                        st.write(answer)

else:

    st.write("""# Smart Organizer""")
    files = st.file_uploader("upload files for data extraction", type=['.png','.jpg','.tif','.pdf','.docx','.txt'], accept_multiple_files=True)

    if 'extarct_files' not in st.session_state:
        st.session_state.extarct_files = False
    if 'extarct_files_submit' not in st.session_state:
        st.session_state.extarct_files_submit = False

    if 'plot_cb_1' not in st.session_state:
        st.session_state.plot_cb_1 = False
    if 'plot_cb_2' not in st.session_state:
        st.session_state.plot_cb_2 = False
    if files or st.session_state.extarct_files:
        st.session_state.extarct_files = True
        st.write("### Files uploaded")
        resp = st.button("Process Images")

        for file in files:
            with open("./uploaded_files/"+file.name,"wb") as f:
                f.write(file.getbuffer())

        if resp or st.session_state.extarct_files_submit:
            st.session_state.extarct_files_submit = True
            my_bar = st.progress(0)
            execute(files)

