#! /bin/sh
sudo apt-get update
sudo apt-get install software-properties-common
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt-get install python3.7 -y
sudo apt install python3-pip -y
sudo apt install rustc -y
apt-get install ffmpeg libsm6 libxext6  -y
sudo apt-get install locales -y
sudo apt-get install language-pack-de
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' | sudo tee /etc/apt/sources.list.d/mongodb.list
sudo apt-get update
sudo apt-get install mongodb-org -y
sudo apt-get install poppler-utils
pip install -r requirement.txt
alias python=python3
python -m nltk.downloader punkt
python get_models.py
unzip text_models.zip
streamlit run app.py